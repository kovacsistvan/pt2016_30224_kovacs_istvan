package tests;

import static org.junit.Assert.*;

import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import controller.Bank;
import model.Account;
import model.Person;
import model.SavingsAccount;



public class Tests {


	/* inserting a quantity over 1000 will return false because of warehouse overstock */
	@Test
	public void depositTest() {
		
		Bank B = new Bank();

		Account b = new SavingsAccount(1, 50, "etc", 1);

		Person p = new Person("a", "b", 1);
		B.addPerson(p);
		B.addSavingsAccount(1, b);
		B.depositAcc(1, 1, 500);
		
		System.out.println(b.getSum());
		
		assertTrue(b.getSum() == 550);
		
	}
	
	@Test
	public void withdrawalTest() {
		
		Bank B = new Bank();
		
		//Account a = new Account(1, 0, "Savings");

		Account b = new SavingsAccount(1, 500, "etc", 1);

		Person p = new Person("a", "b", 1);
		B.addPerson(p);
		B.addSavingsAccount(1, b);
		B.withdrawAcc(1, 1, 50);
		
		System.out.println(b.getSum());
		
		assertTrue(b.getSum() == 450);
		
	}

}
