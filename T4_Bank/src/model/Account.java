package model;

import java.io.Serializable;

import controller.Observer;

public class Account implements Serializable {

	private int accountId;
	private int sum;
	private String category;
	private Observer observer;
	
	public Account(int accountId, int sum, String category) {
		this.accountId = accountId;
		this.sum = sum;
		this.category = category;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
		notifyAllObservers();
	}
	public int getSum() {
		return sum;
	}
	public void setSum(int sum) {
		this.sum = sum;
		notifyAllObservers();
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
		notifyAllObservers();
	}
	
	public void attach(Observer observer) {
		this.observer = observer;
	}

	public void notifyAllObservers() {
		observer.update();
	}
}
