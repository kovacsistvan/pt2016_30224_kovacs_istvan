package model;

import java.io.Serializable;

public class SpendingAccount extends Account implements Serializable{

	private int threshold;

	public SpendingAccount(int accountId, int sum, String category,int threshold) {
		super(accountId, sum, "SpendingAcc");
		this.threshold = threshold;
	}

	public int getThreshold() {
		return threshold;
	}

	public void setThreshold(int threshold) {
		this.threshold = threshold;
	}
	
	
}
