package model;

import java.io.Serializable;

public class SavingsAccount extends Account implements Serializable{

	private int interest;
	
	public SavingsAccount(int accountId, int sum, String category,int interest) {
		super(accountId, sum, "SavingsAcc");
		this.interest = interest;
	}

	public int getInterest() {
		return interest;
	}

	public void setInterest(int interest) {
		this.interest = interest;
	}
	
	
}
