package controller;


import java.io.Serializable;

import model.Account;

public class ObserverDP extends Observer implements Serializable {

	public ObserverDP(Account account) {
		this.account = account;
		this.account.attach(this);
	}

	@Override
	public void update() {
		System.out.println("S-au efectuat modificari in contul " + account.getAccountId() + ".");
	}

}
