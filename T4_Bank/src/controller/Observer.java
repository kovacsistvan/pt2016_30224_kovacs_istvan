package controller;

import model.Account;

public abstract class Observer {
	public Account account;

	public abstract void update();

}

