package controller;

import javax.swing.table.DefaultTableModel;

import model.Account;
import model.Person;

public interface BankProc {
	
	/**
	 * 
	 * @param id
	 * @param idAcc
	 * @param x
	 * @pre id != 0 && idAcc != 0;
	 * @post sumF > sum
	 */
	public void depositAcc(int id, int idAcc, int x);	
	
	/**
	 * 
	 * @param id
	 * @param idAcc
	 * @param x
	 * @pre id != 0 && idAcc != 0;
	 * @post sumF < sum
	 */
	public void withdrawAcc(int id, int idAcc, int x);
	
	
	/**
	 * 
	 * @param p
	 * 
	 * @pre p!=null
	 * @post p.getId() < 1000
	 */
	public void addPerson(Person p);
	
	/**
	 * 
	 * @param id
	 * 
	 * @pre id!=0
	 * @post p.getId() < 1000
	 */
	public void removePerson(int id);
	
	/**
	 * 
	 * @param id
	 * @param savingAcc
	 * @pre id!=0 && savingAcc.getSum() >=0
	 * @post savingAcc.getId() > 0
	 */
	public void addSavingsAccount(int id, Account savingAcc);
	
	/**
	 * 
	 * @param id
	 * @param spendingAcc
	 * @pre id!=0 && spendingAcc.getSum() >=0
	 * @post spendingAcc.getId() > 0
	 */
	public void addSpendingAccount(int id, Account spendingAcc);	

	/**
	 * 
	 * @param person
	 * @param acc
	 * @pre id!=0 && savingAcc.getSum() >=0
	 * @post acc > 0
	 */
	public void removeAccount(int person, int acc);
	
	/**
	 * 
	 * @param id
	 * @pre id!=0
	 * @post id!=0
	 */
	public DefaultTableModel redrawAccount(int id);
	
	
	public DefaultTableModel redrawTable();
}
