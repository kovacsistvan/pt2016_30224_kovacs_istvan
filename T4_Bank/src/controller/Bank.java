package controller;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.table.DefaultTableModel;

import model.Account;
import model.Person;
import model.SavingsAccount;
import model.SavingsAccount;

public class Bank implements BankProc, Serializable  {

	private ConcurrentHashMap<Person, ArrayList<Account>> bank = new ConcurrentHashMap<Person, ArrayList<Account>>();

	public DefaultTableModel redrawAccount(int id) {
		assert isWellFormed();
		assert id!=0;
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("Account ID");
		model.addColumn("Type");
		model.addColumn("Sum");

		Iterator<Map.Entry<Person, ArrayList<Account>>> entries = bank.entrySet().iterator();

		while (entries.hasNext()) {

			Map.Entry<Person, ArrayList<Account>> entry = entries.next();
			if (entry.getKey().getId() == id) {

				for (Account account : bank.get(entry.getKey()))
					if (account != null)
						model.addRow(new Object[] { account.getAccountId(), account.getCategory(), account.getSum() });
			}
		}
		assert isWellFormed();
		assert id !=0;
		return model;
	}

	public DefaultTableModel redrawTable() {
		assert isWellFormed();
		DefaultTableModel model = new DefaultTableModel();

		model.addColumn("First Name");
		model.addColumn("Last Name");
		model.addColumn("ID");

		Iterator<Map.Entry<Person, ArrayList<Account>>> entries = bank.entrySet().iterator();

		while (entries.hasNext()) {
			Map.Entry<Person, ArrayList<Account>> entry = entries.next();

			model.addRow(new Object[] { entry.getKey().getFirstName(), entry.getKey().getLastName(),
					entry.getKey().getId() });

		}
		assert isWellFormed();
		return model;
	}

	public void addPerson(Person p) {
		assert isWellFormed();
		assert p != null;
		ArrayList<Account> acc = new ArrayList<Account>();
		bank.put(p, acc);

		//System.out.println("person added");
		assert isWellFormed();
		assert p.getId() < 1000;
	}

	public void removePerson(int id) {
		assert isWellFormed();
		assert id != 0;
		Iterator<Map.Entry<Person, ArrayList<Account>>> entries = bank.entrySet().iterator();

		while (entries.hasNext()) {
			Map.Entry<Person, ArrayList<Account>> entry = entries.next();
			if (entry.getKey().getId() == id) {
				entries.remove();
			}

		}
		assert isWellFormed();

		assert id < 1000;
	}

	public void addSavingsAccount(int id, Account savingAcc) {
		assert isWellFormed();
		assert id!=0 && savingAcc.getSum() >=0;

		new ObserverDP(savingAcc);
		//savingAcc.notifyAllObservers();
		Iterator<Map.Entry<Person, ArrayList<Account>>> entries = bank.entrySet().iterator();
		savingAcc.setSum(savingAcc.getSum());
		while (entries.hasNext()) {
			Map.Entry<Person, ArrayList<Account>> entry = entries.next();
			if (entry.getKey().getId() == id) {
				bank.get(entry.getKey()).add(savingAcc);
			//	System.out.println("account added");
			}
		}
		assert isWellFormed();
		assert savingAcc.getAccountId() > 0;
	}

	public void addSpendingAccount(int id, Account spendingAcc) {
		assert isWellFormed();
		assert id!=0 && spendingAcc.getSum() >=0;
		new ObserverDP(spendingAcc);
		Iterator<Map.Entry<Person, ArrayList<Account>>> entries = bank.entrySet().iterator();

		while (entries.hasNext()) {
			Map.Entry<Person, ArrayList<Account>> entry = entries.next();
			if (entry.getKey().getId() == id) {
				bank.get(entry.getKey()).add(spendingAcc);
				//System.out.println("account added");
			}

		}
		assert isWellFormed();
		assert spendingAcc.getAccountId() > 0;
	}

	public void removeAccount(int person, int acc) {
		assert isWellFormed();
		assert person!=0 && acc >=0;
		Iterator<Map.Entry<Person, ArrayList<Account>>> entries = bank.entrySet().iterator();

		while (entries.hasNext()) {
			Map.Entry<Person, ArrayList<Account>> entry = entries.next();
			if (entry.getKey().getId() == person) {

				for (Account a : bank.get(entry.getKey())) {

					if (a.getAccountId() == acc) {

						bank.get(entry.getKey()).remove(acc);
					}
				}
			}
		}
		assert isWellFormed();
		assert acc >= 0;
	}

	public void depositAcc(int id, int idAcc, int x) {
		assert isWellFormed();
		assert id != 0 && idAcc != 0;
		int sum = x;
		int sumF = 0;
		Iterator<Map.Entry<Person, ArrayList<Account>>> entries = bank.entrySet().iterator();

		while (entries.hasNext()) {
			Map.Entry<Person, ArrayList<Account>> entry = entries.next();
			if (entry.getKey().getId() == id) {

				for (Account a : bank.get(entry.getKey())) {

					if (a.getAccountId() == idAcc) {

						new ObserverDP(a);
						a.setSum(a.getSum() + x);
						System.out.println("deposited " + x + " total = " + a.getSum());
						sumF = a.getSum() + x;
					}
				}
			}

		}
		assert isWellFormed();
		assert sumF > sum;
	}

	public void withdrawAcc(int id, int idAcc, int x) {
		assert isWellFormed();	assert id != 0 && idAcc != 0;
		int sum = x;
		int sumF = 0;
		Iterator<Map.Entry<Person, ArrayList<Account>>> entries = bank.entrySet().iterator();

		while (entries.hasNext()) {
			Map.Entry<Person, ArrayList<Account>> entry = entries.next();
			if (entry.getKey().getId() == id) {

				for (Account a : bank.get(entry.getKey())) {

					if (a.getAccountId() == idAcc) {

						new ObserverDP(a);
						if (a.getSum() - x >= 0)
							a.setSum(a.getSum() - x);
						else
							System.out.println("negative amount");
					//	System.out.println("withdrawn " + x + " total = " + a.getSum());
						sumF = a.getSum() - x;
					}
				}
			}

		}
		assert isWellFormed();
		assert sumF < sum;
	}
	
	public boolean isWellFormed() {
		return this.bank != null;
	}

	public ConcurrentHashMap<Person, ArrayList<Account>> getBank() {
		return bank;
	}

	public void setBank(ConcurrentHashMap<Person, ArrayList<Account>> bank) {
		this.bank = bank;
	}
	
	public void store(ConcurrentHashMap<Person, ArrayList<Account>> map){
		
		try {
			FileOutputStream fileOut = new FileOutputStream("C:\\Users\\user\\Desktop\\map.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			out.writeObject(map);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
		
	}
	
	public void load(){
		 try (
		            InputStream file = new FileInputStream("C:\\Users\\user\\Desktop\\map.ser");
		            InputStream buffer = new BufferedInputStream(file);
		            ObjectInput input = new ObjectInputStream(buffer);) {

		    	this.setBank((ConcurrentHashMap<Person, ArrayList<Account>>) input.readObject());

		    } catch (ClassNotFoundException ex) { } 
		    catch (IOException ex) { }
	}

}
