package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import controller.Bank;
import model.Account;
import model.Person;
import model.SavingsAccount;
import model.SpendingAccount;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.awt.event.ActionEvent;
import javax.swing.JSeparator;

public class Interface extends JFrame implements Serializable {

	private JPanel contentPane;
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_5;
	private JTextField textField_7;
	private JTable table_1;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */

	public Interface() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 617, 577);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 255, 204));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(258, 22, 327, 179);
		contentPane.add(scrollPane);
		
		Bank B = new Bank();
		
		table = new JTable();
		DefaultTableModel model = new DefaultTableModel();
		model.addColumn("First Name");
		model.addColumn("Last Name");
		model.addColumn("ID");

		table.setModel(model);
		scrollPane.setViewportView(table);

		textField = new JTextField();
		textField.setBounds(97, 20, 129, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(97, 51, 129, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(97, 102, 129, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(10, 23, 77, 14);
		contentPane.add(lblFirstName);

		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 54, 77, 14);
		contentPane.add(lblLastName);
		
		B.load();
		table.setModel(B.redrawTable());
		
		JButton btnAddClient = new JButton("Add Client");
		btnAddClient.setBackground(Color.LIGHT_GRAY);

		btnAddClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Person p = new Person(textField.getText(), textField_1.getText(),
						Integer.parseInt(textField_2.getText()));
				B.addPerson(p);

				table.setModel(B.redrawTable());
				B.store(B.getBank());

			}
		});

		btnAddClient.setBounds(97, 145, 129, 23);
		contentPane.add(btnAddClient);

		JButton btnSelectClient = new JButton("Remove Client");
		btnSelectClient.setBackground(Color.LIGHT_GRAY);
		btnSelectClient.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				B.removePerson(Integer.parseInt(textField_2.getText()));
				table.setModel(B.redrawTable());
				B.store(B.getBank());	
			}
		});
		btnSelectClient.setBounds(97, 179, 129, 23);
		contentPane.add(btnSelectClient);

		JLabel lblClientId = new JLabel("Client ID");
		lblClientId.setBounds(10, 105, 46, 14);
		contentPane.add(lblClientId);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(258, 224, 333, 289);
		contentPane.add(scrollPane_1);

		table_1 = new JTable();

		DefaultTableModel model1 = new DefaultTableModel();

		model1.addColumn("Account ID");
		model1.addColumn("Type");
		model1.addColumn("Sum");

		table_1.setModel(model1);

		table_1.setModel(B.redrawAccount(1));
		
		scrollPane_1.setViewportView(table_1);

		JSeparator separator = new JSeparator();
		separator.setBounds(268, 212, 323, 2);
		contentPane.add(separator);

		JButton btnSavingsacc = new JButton("SavingsAcc");
		btnSavingsacc.setBackground(Color.LIGHT_GRAY);
		btnSavingsacc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				B.addSavingsAccount(Integer.parseInt(textField_2.getText()),
						new SavingsAccount(Integer.parseInt(textField_7.getText()),
								Integer.parseInt(textField_5.getText()), "ctg",5));

				table_1.setModel(B.redrawAccount(Integer.parseInt(textField_2.getText())));
				B.store(B.getBank());
			}
		});
		btnSavingsacc.setBounds(97, 347, 129, 23);
		contentPane.add(btnSavingsacc);

		textField_5 = new JTextField();
		textField_5.setBounds(97, 282, 129, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);

		JLabel lblSum = new JLabel("Sum");
		lblSum.setBounds(10, 285, 46, 14);
		contentPane.add(lblSum);

		JButton btnSpendingacc = new JButton("SpendingAcc");
		btnSpendingacc.setBackground(Color.LIGHT_GRAY);
		btnSpendingacc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				B.addSpendingAccount(Integer.parseInt(textField_2.getText()),
						new SpendingAccount(Integer.parseInt(textField_7.getText()),
								Integer.parseInt(textField_5.getText()), "ctg",5));
				
				table_1.setModel(B.redrawAccount(Integer.parseInt(textField_2.getText())));
				B.store(B.getBank());
			}
		});
		
		
		
		btnSpendingacc.setBounds(97, 313, 129, 23);
		contentPane.add(btnSpendingacc);

		textField_7 = new JTextField();
		textField_7.setColumns(10);
		textField_7.setBounds(97, 251, 129, 20);
		contentPane.add(textField_7);

		JLabel lblAccountId = new JLabel("Account ID");
		lblAccountId.setBounds(10, 249, 77, 25);
		contentPane.add(lblAccountId);

		JButton btnViewacc = new JButton("View Accounts");
		btnViewacc.setBackground(Color.LIGHT_GRAY);
		btnViewacc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				table_1.setModel(B.redrawAccount(Integer.parseInt(textField_2.getText())));
				B.store(B.getBank());
				
			}
		});
		btnViewacc.setBounds(97, 213, 129, 23);
		contentPane.add(btnViewacc);
		
		JButton btnDeposit = new JButton("Deposit");
		btnDeposit.setBackground(Color.LIGHT_GRAY);
		
		btnDeposit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				B.depositAcc(Integer.parseInt(textField_2.getText()), Integer.parseInt(textField_7.getText()),
						Integer.parseInt(textField_3.getText()));
				table_1.setModel(B.redrawAccount(Integer.parseInt(textField_2.getText())));
				B.store(B.getBank());
				
			}
		});
		btnDeposit.setBounds(97, 422, 129, 23);
		contentPane.add(btnDeposit);
		
		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.setBackground(Color.LIGHT_GRAY);
		btnWithdraw.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					
					B.withdrawAcc(Integer.parseInt(textField_2.getText()), Integer.parseInt(textField_7.getText()),
							Integer.parseInt(textField_3.getText()));
				table_1.setModel(B.redrawAccount(Integer.parseInt(textField_2.getText())));
				B.store(B.getBank());
					
			}
		});
		btnWithdraw.setBounds(97, 456, 129, 23);
		contentPane.add(btnWithdraw);
		
		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setBounds(10, 390, 46, 14);
		contentPane.add(lblAmount);
		
		textField_3 = new JTextField();
		textField_3.setBounds(97, 387, 129, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnNewButton = new JButton("Remove Acc");
		btnNewButton.setBackground(Color.LIGHT_GRAY);
		btnNewButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				
				B.removeAccount(Integer.parseInt(textField_2.getText()), Integer.parseInt(textField_7.getText()));
				table_1.setModel(B.redrawAccount(Integer.parseInt(textField_2.getText())));
				B.store(B.getBank());
			}
			
		});
		btnNewButton.setBounds(97, 490, 129, 23);
		contentPane.add(btnNewButton);
		
	}
}
