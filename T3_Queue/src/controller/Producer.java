package controller;

/*
 * Adds Customers to the queue by using a BlockingQueue
 * 
 * 
 * 
 * Kovacs Istvan-Norbert, kovacsistvan1202@gmail.com
 */

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.BlockingQueue;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;

import model.Customer;

public class Producer implements Runnable {
	
	private static final Logger log= Logger.getLogger( Producer.class.getName() );

	private BlockingQueue<Customer> queue;
	private DefaultListModel<Customer> model;
	private JList<Customer> list;
	private int min;
	private int max;
	private int duration;
	private int i=0;
	private int waitingTime=0;
	private int emptyTime;
	private JLabel label;
	private JLabel emptyLabel;
	private JLabel peakLabel;
	private int maxSize=0;
	
	public Producer(BlockingQueue<Customer> q, JList<Customer> l, DefaultListModel<Customer> model,
			int min, int max, int duration, JLabel label, JLabel emptyLabel, JLabel peakLabel) {
		this.queue = q;
		this.list = l;
		this.min = min;
		this.max = max;
		this.model = model;
		this.duration = duration;
		this.label = label;
		this.emptyLabel = emptyLabel;
		this.peakLabel = peakLabel;
	}
	@Override
	public void run() {
		try {
			
		long start = System.currentTimeMillis();
		long time=0;
		model.removeAllElements();
		list.setModel(model);
		
		FileHandler fh = new FileHandler("D:/workspace java/Queue/logfile1.txt");  
        log.addHandler(fh);
        
        SimpleFormatter formatter = new SimpleFormatter();  
        fh.setFormatter(formatter);  
		
        maxSize = 0;
        
		while (time / 1000  <= duration ){
			time = System.currentTimeMillis() - start;
			
			Customer C = new Customer();

	    	 int wait = min + (int)(Math.random() * ((max - min) + 1));
	    	 waitingTime += wait;
	    	 i++;
	    	 
	    	 if (model.isEmpty())
					emptyTime += wait;
	    	 
	    	 Thread.sleep(wait * 1000);
	 
	    	 if (model.size() > maxSize) maxSize = model.size();
	    	 
	    	 queue.put(C);
	    	 model.addElement(C);
	    	 list.setModel(model);
	    	 log.info("Producer: " + C.getName()  +  " produced"); 
	    	// System.out.println("Producer: " + C.getName()  +  " produced");
	    	 //	log.log( Level.FINE, "Producer: " + C.getName()  +  " produced");
		 }
		 } 
	
		catch (FileNotFoundException | UnsupportedEncodingException | InterruptedException e) { } catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int averageWait = waitingTime / i;
		i=0;
		waitingTime=0;
		
		label.setText("" + averageWait);
		emptyLabel.setText("" + emptyTime);
		peakLabel.setText("Peak time had " + maxSize + " customers in queue.");
		
		Customer blocked = new Customer("Closed");
		
		model.removeAllElements();
		model.addElement(blocked);
		list.setModel(model);

	}
	
	public DefaultListModel<Customer> getModel() {
		return model;
	}
	public void setModel(DefaultListModel<Customer> model) {
		this.model = model;
	}
	public JList<Customer> getList() {
		return list;
	}
	public void setList(JList<Customer> list) {
		this.list = list;
	}
	
	
}
