package controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.BlockingQueue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;

import model.Customer;

/*
 * Takes Customers to the queue by using a BlockingQueue
 * 
 * 
 * 
 * Kovacs Istvan-Norbert, kovacsistvan1202@gmail.com
 */

public class Consumer implements Runnable {
	
	private static final Logger log= Logger.getLogger( Consumer.class.getName() );
	
	private BlockingQueue<Customer> queue;
	private DefaultListModel<Customer> model;
	private JList<Customer> list;
	private int min;
	private int max;
	private int duration;
	private int i=0;
	private int serviceTime=0;
	private JLabel label_1;
	
	public Consumer(BlockingQueue<Customer> q, JList<Customer> l, DefaultListModel<Customer> model,
			int min, int max, int duration,JLabel label_1) {
		this.queue = q;
		this.list = l;
		this.min = min;
		this.max = max;
		this.model = model;
		this.duration = duration;
		this.label_1 = label_1;
	}
	@Override
	public void run() {
		try {
			
			FileHandler fh = new FileHandler("D:/workspace java/Queue/logfile.txt");  
	        log.addHandler(fh);
	        
	        SimpleFormatter formatter = new SimpleFormatter();  
	        fh.setFormatter(formatter);  
	        
			long start = System.currentTimeMillis();
			long time=0;
			
			Customer C = new Customer();
			model.removeAllElements();
			list.setModel(model);
			// consuming messages until exit message is received
			while (time / 1000  <= duration ){
				time = System.currentTimeMillis() - start;
				int wait = min + (int)(Math.random() * ((max - min) + 1));
				 serviceTime += wait;
		    	 i++;
				Thread.sleep(wait * 1000);
				C.setName(queue.take().getName());
				
				if (model.getSize() > 0) 		
				model.removeElement(model.firstElement());
						
				log.info("Consumer: " + C.getName() +  " consumed" + Thread.currentThread());
			 }
			 } 
		
			catch (FileNotFoundException | UnsupportedEncodingException | InterruptedException e) { } catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		int averageService= serviceTime / i;
		i=0;
		serviceTime=0;
		
		label_1.setText("" + averageService);
		
		model.removeAllElements();
		Customer blocked = new Customer("Closed");
		
		model.removeAllElements();
		model.addElement(blocked);
		list.setModel(model);
		list.setModel(model);
		
	}

}