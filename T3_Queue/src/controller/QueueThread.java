package controller;

public class QueueThread {

	public static void main(String[] args) {
		Thread thread1 = new Thread(new Thread(), "thread1");
		Thread thread2 = new Thread(new Thread(), "thread2");
		Thread thread3 = new Thread("thread3");
		//Start the threads
		thread1.start();
		thread2.start();
		try {
			//delay for one second
			Thread.currentThread().sleep(1000);
		} catch (InterruptedException e) {
		}
		//Display info about the main thread
		System.out.println(Thread.currentThread());
	}
}
