package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.Consumer;
import controller.Producer;
import model.Customer;

import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import java.awt.SystemColor;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import javax.swing.JTabbedPane;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/*
 * Serves as a launcher and interface.
 * 
 * 
 * 
 * Kovacs Istvan-Norbert, kovacsistvan1202@gmail.com
 */

public class GUI extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setTitle("Queue Manager");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 665, 342);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBackground(new Color(192, 192, 192));
		tabbedPane.setBounds(0, 0, 649, 303);
		contentPane.add(tabbedPane);
		
		
		
		BlockingQueue<Customer> q = new ArrayBlockingQueue<Customer>(10);
		
	     
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(new Color(192, 192, 192));
		tabbedPane.addTab("Queues", null, panel_1, null);
		panel_1.setLayout(null);
		
		DefaultListModel<Customer> model = new DefaultListModel<Customer>();
		DefaultListModel<Customer> model1 = new DefaultListModel<Customer>();
		DefaultListModel<Customer> model2 = new DefaultListModel<Customer>();
		DefaultListModel<Customer> model3 = new DefaultListModel<Customer>();
		JList<Customer> list = new JList<Customer>();
		list.setBackground(new Color(204, 255, 204));
		list.setBounds(10, 11, 89, 253);
		panel_1.add(list);
		
		JList list_1 = new JList();
		list_1.setBackground(new Color(204, 255, 204));
		list_1.setBounds(109, 11, 89, 253);
		panel_1.add(list_1);
		
		JList list_2 = new JList();
		list_2.setBackground(new Color(204, 255, 204));
		list_2.setBounds(208, 11, 89, 253);
		panel_1.add(list_2);
		
		JList list_3 = new JList();
		list_3.setBackground(new Color(204, 255, 204));
		list_3.setBounds(307, 11, 89, 253);
		panel_1.add(list_3);
		
		textField = new JTextField();
		textField.setBounds(524, 9, 70, 20);
		panel_1.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(524, 40, 70, 20);
		panel_1.add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setColumns(10);
		textField_2.setBounds(524, 73, 70, 20);
		panel_1.add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(524, 104, 70, 20);
		panel_1.add(textField_3);
		
		JLabel lblMinService = new JLabel("Min. service ");
		lblMinService.setBounds(422, 76, 92, 14);
		panel_1.add(lblMinService);
		
		JLabel lblMaxService = new JLabel("Max. service");
		lblMaxService.setBounds(422, 107, 92, 14);
		panel_1.add(lblMaxService);
		
		JLabel lblMinWait = new JLabel("Min. wait");
		lblMinWait.setBounds(422, 12, 55, 14);
		panel_1.add(lblMinWait);
		
		JLabel lblMaxWait = new JLabel("Max. wait");
		lblMaxWait.setBounds(422, 45, 70, 14);
		panel_1.add(lblMaxWait);
		
		textField_4 = new JTextField();
		textField_4.setBounds(524, 154, 72, 20);
		panel_1.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblDuration = new JLabel("Duration");
		lblDuration.setBounds(422, 157, 92, 14);
		panel_1.add(lblDuration);
		
		textField_5 = new JTextField();
		textField_5.setBounds(524, 185, 70, 20);
		panel_1.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblNrOfQueues = new JLabel("Nr. of queues");
		lblNrOfQueues.setBounds(422, 188, 89, 14);
		panel_1.add(lblNrOfQueues);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Statistics", null, panel, null);
		panel.setBackground(new Color(192, 192, 192));
		panel.setLayout(null);
		
		JLabel lblAverageWaitingTime = new JLabel("Average waiting time:");
		lblAverageWaitingTime.setBounds(43, 28, 127, 14);
		panel.add(lblAverageWaitingTime);
		
		JLabel lblAverageServiceTime = new JLabel("Average service time:");
		lblAverageServiceTime.setBounds(43, 89, 127, 14);
		panel.add(lblAverageServiceTime);
		
		JLabel label = new JLabel("");
		label.setBackground(new Color(255, 255, 255));
		label.setBounds(207, 28, 85, 14);
		panel.add(label);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(207, 89, 85, 14);
		panel.add(label_1);
		
		JLabel lblTimeSpentEmpty = new JLabel("Avg. time spent empty:");
		lblTimeSpentEmpty.setBounds(43, 136, 154, 14);
		panel.add(lblTimeSpentEmpty);
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(207, 136, 85, 14);
		panel.add(label_2);
		
		JLabel lblPeakTime = new JLabel("Peak time:");
		lblPeakTime.setBounds(43, 180, 145, 14);
		panel.add(lblPeakTime);
		
		JLabel label_3 = new JLabel("");
		label_3.setBounds(207, 180, 210, 14);
		panel.add(label_3);
		
		
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int minWait = Integer.parseInt(textField.getText());
				int maxWait = Integer.parseInt(textField_1.getText());
				int minServ = Integer.parseInt(textField_2.getText());
				int maxServ = Integer.parseInt(textField_3.getText());
				int duration = Integer.parseInt(textField_4.getText());
				int queueNr = Integer.parseInt(textField_5.getText());
				
				 Producer p = new Producer(q,list,model,minWait,maxWait,duration,label,label_2,label_3);
			     Consumer c = new Consumer(q,list,model,minServ,maxServ,duration,label_1);
			     Producer p1 = new Producer(q,list_1,model1,minWait,maxWait,duration,label,label_2,label_3);
			     Consumer c1 = new Consumer(q,list_1,model1,minServ,maxServ,duration,label_1);
			     Producer p2 = new Producer(q,list_2,model2,minWait,maxWait,duration,label,label_2,label_3);
			     Consumer c2 = new Consumer(q,list_2,model2,minServ,maxServ,duration,label_1);
			     Producer p3 = new Producer(q,list_3,model3,minWait,maxWait,duration,label,label_2,label_3);
			     Consumer c3 = new Consumer(q,list_3,model3,minServ,maxServ,duration,label_1);
			     
			     switch (queueNr) {
			     
			     case 4: new Thread(p3).start();    
			     		 new Thread(c3).start();
			    
			     case 3: new Thread(p2).start();    
			     		 new Thread(c2).start();
			    	 
			     case 2: new Thread(p1).start();    
			     		 new Thread(c1).start();
			    	 
			     case 1: new Thread(p).start();    
			     	     new Thread(c).start();
			     	     break;
			     default: break;
			     }

			     System.out.println("start!");

			}
		});
		
		btnStart.setBounds(457, 227, 89, 23);
		panel_1.add(btnStart);
		
	}
}
