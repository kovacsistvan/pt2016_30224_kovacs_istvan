package tests;

import static org.junit.Assert.*;

import javax.swing.DefaultListModel;

import org.junit.Test;

import model.Customer;

public class TestCase {

	@Test
	public void conditionalTest1() {

		long start = System.currentTimeMillis() / 1000;
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		long time = System.currentTimeMillis()/1000 - start;
		
		System.out.println(time);
		assertTrue(time  <= 2);
	}
	
	@Test
	public  void sizeTest() {
		
		DefaultListModel model = new DefaultListModel();
		model.setSize(5);
		
		assertTrue(model.size() == 5);
		
	}

}
