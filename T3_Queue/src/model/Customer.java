package model;

/*
 * Implements the customer class. A customer has a random name chosen out of the list.
 * 
 * 
 * 
 * Kovacs Istvan-Norbert, kovacsistvan1202@gmail.com
 */

public class Customer {

	private String name;
	
	private String names[] = { "Harrison", "James", "Davis", "Bennett", "Richards", " Dixon", "Spencer", "Newman",
			"Gregory", "Newton", "Freeman", "Griffin", "Higgins", "Stephenson", "Lynch", "Norman", "Alexander", "Fuller", "Glover",
			"Atkins", "Clements", "Randall", "Carpenter", "Randall", "Pickering", "Hurst", "Peacock", "Brady", "Bruce", "Ingram",
			"Leonard", "Godfrey", "Nixon", "Winters"};

	public Customer(String name) {
		this.name = name;
	}
	
	public Customer() {
		this.name = names[(int)(Math.random() * 34)];
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		return "" + name + "";
	}
	
	
	
}
