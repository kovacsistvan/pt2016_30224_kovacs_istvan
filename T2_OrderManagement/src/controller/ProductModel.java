package controller;

import java.awt.List;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeMap;

import model.Customer;
import model.Product;
import model.Warehouse;

import javax.swing.RowFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.table.DefaultTableModel;

/*
 * implements methods for the Product JTable
 * 
 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */

public class ProductModel extends AbstractTableModel {
/* products that populate the model */
	ArrayList<Product> warehouse;

	private String[] columnNames = { "Name", "Category", "Quantity", "Price" };

	public ProductModel() {
		warehouse = new ArrayList<Product>();
	}

	public ProductModel(ArrayList<Product> warehouse) {

		this.warehouse = warehouse;
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getColumnCount() {

		return columnNames.length;
	}

	@Override
	public int getRowCount() {

		return warehouse.size();
	}

	@Override
	public Object getValueAt(int row, int column) {
		Product product = getProduct(row);

		if (product != null) {

			switch (column) {
			case 0:
				return product.getName();
			case 1:
				return product.getCategory();
			case 2:
				return product.getQuantity();
			case 3:
				return product.getPrice();
			default:
				return null;
			}
		} else
			return null;
	}

	@Override
	public boolean isCellEditable(int row, int column) {
		switch (column) {
		case 2:
			return true;
		case 3:
			return true;
		default:
			return false;
		}

	}

	public Product getProduct(int row) {
		return warehouse.get(row);
	}

	public void addProduct(Product product) {
		if (product != null)
		insertProduct(getRowCount(), product);

	}

	public void insertProduct(int row, Product product)

	{
		warehouse.add(row, product);
		fireTableRowsInserted(row, row);

	}

	public void removeProduct(int row) {
		warehouse.remove(row);
		fireTableRowsDeleted(row, row);

	}
	
	public void loadProducts() throws IOException, ClassNotFoundException {

	    try (
	            InputStream file = new FileInputStream("C:\\Users\\user\\Desktop\\products.ser");
	            InputStream buffer = new BufferedInputStream(file);
	            ObjectInput input = new ObjectInputStream(buffer);) {

	    	ArrayList<Product> recoveredProducts = (ArrayList<Product>) input.readObject();
	
	       warehouse = recoveredProducts;

	    } catch (ClassNotFoundException ex) { } 
	    catch (IOException ex) { }
	}
	
	public void storeProducts(ArrayList<Product> warehouse){
		
		try {
			FileOutputStream fileOut = new FileOutputStream("C:\\Users\\user\\Desktop\\products.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			out.writeObject(warehouse);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
	
	Comparator<String> comparator = new Comparator<String>() {

		public int compare(String s1, String s2) {
			String[] strings1 = s1.split("\\s");
			String[] strings2 = s2.split("\\s");
			return strings1[strings1.length - 1].compareTo(strings2[strings2.length - 1]);
		}
	};

	public ArrayList<Product> getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(ArrayList<Product> warehouse) {
		this.warehouse = warehouse;
	}

	
	
	
}
