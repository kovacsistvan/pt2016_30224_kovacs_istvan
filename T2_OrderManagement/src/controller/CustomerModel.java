package controller;

import java.awt.List;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.util.ArrayList;
import java.util.Comparator;

import javax.swing.RowFilter;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import java.io.FileInputStream;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import model.Customer;
/*
 * implements methods for the Customer JTable
 * 
 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */
public class CustomerModel extends AbstractTableModel{
/* customers that populate the model */
	private ArrayList<Customer> customers;
	
	 private String[] columnNames =
		    {
		        "First Name",
		        "Last Name",
		        "Customer ID"
		    };
	 
	 public CustomerModel(){	 
		 customers= new ArrayList<Customer>();	 
	 }
	 
	 public CustomerModel(ArrayList<Customer> customers){	 
		 this.customers = customers;	 
	 }
	 
	 @Override
	 public String getColumnName(int column) {
		  return columnNames[column];
		}
	 
	@Override
	public int getColumnCount() {

		return columnNames.length;
	}

	@Override
	public int getRowCount() {

		return customers.size();
	}

	@Override
	public Object getValueAt(int row, int column)
	{
	    Customer person = getCustomer(row);
	 
	    switch (column)
	    {
	        case 0: return person.getFirstName();
	        case 1: return person.getLastName();
	        case 2: return person.getCustomerId();
	        default: return null;
	    }
	  
	}


	
	@Override
	public boolean isCellEditable(int row, int column)
	{
	    switch (column)
	    {
	        case 2: return false;
	        default: return true;
	    }
	    
	}
	
	public Customer getCustomer(int row)
	{
	    return customers.get( row );
	}
	
	public void addCustomer(Customer person)
	{
	    insertCustomer(getRowCount(), person);
	}
	
	public void insertCustomer(int row, Customer person)
	{
	    customers.add(row, person);
	    fireTableRowsInserted(row, row);
	}
	
	public void removeCustomer(int row)
	{
	    customers.remove(row);
	    fireTableRowsDeleted(row, row);
	    
	}	
	
	public void loadCustomers() throws IOException, ClassNotFoundException {

	    try (
	            InputStream file = new FileInputStream("C:\\Users\\user\\Desktop\\contacts.ser");
	            InputStream buffer = new BufferedInputStream(file);
	            ObjectInput input = new ObjectInputStream(buffer);) {
	        //deserialize the List
	    	ArrayList<Customer> recoveredCustomers = (ArrayList<Customer>) input.readObject();
	        //display its data
	
	       customers = recoveredCustomers;

	    } catch (ClassNotFoundException ex) { } 
	    catch (IOException ex) { }
	}
	
	public void storeCustomers(ArrayList<Customer> customers){
		
		try {
			FileOutputStream fileOut = new FileOutputStream("C:\\Users\\user\\Desktop\\contacts.ser");
			ObjectOutputStream out = new ObjectOutputStream(fileOut);

			out.writeObject(customers);
			out.close();
			fileOut.close();
		} catch (IOException i) {
			i.printStackTrace();
		}
	}
	
	Comparator<String> comparator = new Comparator<String>() {
		
	    public int compare(String s1, String s2) {
	        String[] strings1 = s1.split("\\s");
	        String[] strings2 = s2.split("\\s");
	        return strings1[strings1.length - 1]
	            .compareTo(strings2[strings2.length - 1]);
	    }
	};

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
	
	
	
}
