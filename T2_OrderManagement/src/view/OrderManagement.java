package view;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import controller.CustomerModel;
import controller.ProductModel;
import model.Customer;
import model.OPDept;
import model.Order;
import model.Product;
import model.Warehouse;

import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.security.KeyStore.Entry;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.awt.event.ActionEvent;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.MatteBorder;

/*
 * implements the GUI, manages the JTable models, listeners, etc.
 * 
 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */

public class OrderManagement extends JFrame implements Serializable {

	/* constitute the GUI's visual elemens */
	private JPanel contentPane;
	private JTable table;
	private JPanel panel_1;
	private JButton btnCreareProdus;
	private JScrollPane scrollPane;
	private JTable table_1;
	private JTable table_2;
	private JScrollBar scrollBar_2;
	private JScrollBar scrollBar;
	private JTextField textField;
	private JTextField textField_5;
	private JTextField textField_1;
	private JTextField textField_1_1;
	private JPanel panel_2;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField filterText;
	private TableRowSorter<CustomerModel> sorter;
	private TableRowSorter<ProductModel> sorter1;
	public static final int WAREHOUSE_THRESHOLD = 1000;
	static int billPrice = 0;
	static int totalStock = 0;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_14;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTable table_3;
	private JTable table_4;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					OrderManagement frame = new OrderManagement();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public OrderManagement() throws ClassNotFoundException, IOException {

		setForeground(new Color(144, 238, 144));
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\user\\Desktop\\briefcase-icon.png"));
		setTitle("Manager comenzi");
		setBackground(new Color(144, 238, 144));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 593, 404);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(144, 238, 144));
		contentPane.setBorder(null);
		setContentPane(contentPane);
		contentPane.setLayout(null);

		TreeMap<String, Product> warehouse = new TreeMap<String, Product>();
		Warehouse W = new Warehouse(warehouse);

		tabbedPane.setFont(new Font("Tahoma", Font.BOLD, 12));
		tabbedPane.setForeground(new Color(0, 0, 0));
		tabbedPane.setBorder(new MatteBorder(1, 1, 1, 1, (Color) new Color(0, 0, 0)));
		tabbedPane.setBackground(new Color(189, 183, 107));
		tabbedPane.setBounds(10, 0, 557, 354);
		contentPane.add(tabbedPane);

		JPanel panel = new JPanel();
		panel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panel.setBackground(new Color(152, 251, 152));
		tabbedPane.addTab("Customers", null, panel, null);
		panel.setLayout(null);

		// MODEL CUSTOMER

		CustomerModel model = new CustomerModel();
		model.loadCustomers();
		textField = new JTextField();
		textField.setBounds(113, 11, 94, 20);
		panel.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(113, 73, 94, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);

		JTable table = new JTable(model);
		table.setBackground(new Color(240, 255, 240));
		this.sorter = new TableRowSorter<CustomerModel>(model);
		table.setRowSorter(sorter);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(251, 0, 289, 300);

		panel.add(scrollPane);

		JButton btnSubmitCustomer = new JButton("Add Customer");
		btnSubmitCustomer.setForeground(Color.BLACK);
		btnSubmitCustomer.setBackground(new Color(169, 169, 169));

		btnSubmitCustomer.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				Customer person = new Customer(textField.getText(), textField_6.getText(),
						Integer.parseInt(textField_1.getText()));
				model.addCustomer(person);
				model.storeCustomers(model.getCustomers());
			}
		});

		btnSubmitCustomer.setBounds(52, 123, 155, 23);

		panel.add(btnSubmitCustomer);

		filterText = new JTextField();
		filterText.setBounds(121, 280, 86, 20);
		panel.add(filterText);
		filterText.setColumns(10);

		textField_6 = new JTextField();
		textField_6.setColumns(10);
		textField_6.setBounds(113, 42, 94, 20);
		panel.add(textField_6);

		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(21, 14, 66, 14);
		panel.add(lblFirstName);

		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(21, 45, 66, 14);
		panel.add(lblLastName);

		JLabel lblCustomerId = new JLabel("Customer ID");
		lblCustomerId.setBounds(21, 76, 82, 14);
		panel.add(lblCustomerId);

		textField_7 = new JTextField();
		textField_7.setBounds(121, 189, 86, 20);
		panel.add(textField_7);
		textField_7.setColumns(10);

		JLabel lblRowToBe = new JLabel("Customer ID:");
		lblRowToBe.setBounds(21, 192, 93, 14);
		panel.add(lblRowToBe);

		JButton btnRemoveRow = new JButton("Remove Client");
		btnRemoveRow.setForeground(Color.BLACK);
		btnRemoveRow.setBackground(new Color(169, 169, 169));
		btnRemoveRow.setBounds(52, 236, 155, 23);
		panel.add(btnRemoveRow);

		JLabel lblCustomerSearch = new JLabel("Customer Search");
		lblCustomerSearch.setBounds(10, 283, 101, 14);
		panel.add(lblCustomerSearch);

		JSeparator separator = new JSeparator();
		separator.setBounds(21, 164, 221, 2);
		panel.add(separator);

		btnRemoveRow.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				for (int x = 0; x < model.getCustomers().size(); x++) {
					if (Integer.parseInt(textField_7.getText()) == model.getCustomers().get(x).getCustomerId()) {
						model.getCustomers().remove(x);
						model.storeCustomers(model.getCustomers());
						model.fireTableDataChanged();
					}
				}
			}
		});

		filterText.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				newFilter();
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				newFilter();
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				newFilter();
			}

		});

		// ======================================================================================
		// PRODUCT

		panel_1 = new JPanel();
		panel_1.setBackground(new Color(144, 238, 144));
		tabbedPane.addTab("Products", null, panel_1, null);

		panel_1.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(213, 8, 327, 304);
		panel_1.add(scrollPane_1);

		ProductModel model1 = new ProductModel();
		//model1.loadProducts();
		JTable table_4 = new JTable(model1);
		scrollPane_1.setViewportView(table_4);
		this.sorter1 = new TableRowSorter<ProductModel>(model1);

		sorter1.setModel(model1);
		table_4.setRowSorter(sorter1);
		btnCreareProdus = new JButton("Add Product");
		btnCreareProdus.setForeground(Color.BLACK);
		btnCreareProdus.setBackground(new Color(169, 169, 169));
		btnCreareProdus.setBounds(33, 144, 170, 23);
		panel_1.add(btnCreareProdus);

		textField_11 = new JTextField();
		textField_11.setBounds(105, 8, 98, 20);
		panel_1.add(textField_11);
		textField_11.setColumns(10);

		textField_12 = new JTextField();
		textField_12.setBounds(105, 39, 98, 20);
		panel_1.add(textField_12);
		textField_12.setColumns(10);

		textField_13 = new JTextField();
		textField_13.setBounds(105, 70, 98, 20);
		panel_1.add(textField_13);
		textField_13.setColumns(10);

		JLabel lblName = new JLabel("Name");
		lblName.setBounds(23, 11, 56, 14);
		panel_1.add(lblName);

		JLabel lblQuantity = new JLabel("Quantity");
		lblQuantity.setBounds(23, 73, 56, 14);
		panel_1.add(lblQuantity);

		JLabel lblPrice = new JLabel("Price");
		lblPrice.setBounds(23, 107, 56, 14);
		panel_1.add(lblPrice);

		JLabel lblCategory = new JLabel("Category");
		lblCategory.setBounds(23, 42, 56, 14);
		panel_1.add(lblCategory);

		textField_14 = new JTextField();
		textField_14.setBounds(105, 101, 98, 20);
		panel_1.add(textField_14);
		textField_14.setColumns(10);

		JLabel lblDeleteRow = new JLabel("Product Name:");
		lblDeleteRow.setBounds(10, 187, 85, 14);
		panel_1.add(lblDeleteRow);

		textField_9 = new JTextField();
		textField_9.setBounds(105, 184, 98, 20);
		panel_1.add(textField_9);
		textField_9.setColumns(10);

		JButton btnRemoveRow_1 = new JButton("Remove Product");
		btnRemoveRow_1.setForeground(Color.BLACK);
		btnRemoveRow_1.setBackground(new Color(169, 169, 169));
		btnRemoveRow_1.setBounds(33, 228, 170, 23);
		panel_1.add(btnRemoveRow_1);

		textField_10 = new JTextField();
		textField_10.setBounds(111, 276, 92, 23);
		panel_1.add(textField_10);
		textField_10.setColumns(10);

		JLabel lblFilter = new JLabel("Product Search");
		lblFilter.setBounds(10, 280, 91, 14);
		panel_1.add(lblFilter);

		JLabel label = new JLabel("");
		label.setForeground(new Color(255, 0, 0));
		label.setFont(new Font("Tahoma", Font.BOLD, 15));
		label.setBounds(22, 119, 188, 23);
		panel_1.add(label);

		Product P1 = new Product("oats", "cereal", 150, 20);
		Product P2 = new Product("barley", "cereal", 210, 25);
		Product P3 = new Product("sheep", "animals", 20, 215);
		Product P4 = new Product("goat", "animals", 30, 300);
		Product P5 = new Product("cow", "animals", 8, 250);
		Product P6 = new Product("horse", "animals", 5, 300);
		Product P7 = new Product("oxen", "animals", 20, 275);
		Product P8 = new Product("pitchfork", "tools", 2, 100);
		Product P9 = new Product("chainsaw", "tools", 5, 300);
		Product P10 = new Product("plow", "tools", 100, 20);

		model1.addProduct(P1);
		model1.addProduct(P2);
		model1.addProduct(P3);
		model1.addProduct(P4);
		model1.addProduct(P5);
		model1.addProduct(P6);
		model1.addProduct(P7);
		model1.addProduct(P8);
		model1.addProduct(P9);
		model1.addProduct(P10);

		W.addProduct(P1);
		W.addProduct(P2);
		W.addProduct(P3);
		W.addProduct(P4);
		W.addProduct(P5);
		W.addProduct(P6);
		W.addProduct(P7);
		W.addProduct(P8);
		W.addProduct(P9);
		W.addProduct(P10);

		btnCreareProdus.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				boolean overstock = true;

				Product product = new Product(textField_11.getText(), textField_12.getText(),
						Integer.parseInt(textField_13.getText()), Integer.parseInt(textField_14.getText()));

				setTotalStock(getTotalStock() + Integer.parseInt(textField_13.getText()));

				overstock = W.addProduct(product);

				if ((!overstock) || getTotalStock() > 3000) {
					label.setText("Warehouse Overstock");
				}

				else {
					label.setText("");
				}

				model1.getWarehouse().clear();

				for (Map.Entry<String, Product> entry : W.getWarehouse().entrySet()) {
					Product p1 = new Product(entry.getValue().getName(), entry.getValue().getCategory(),
							entry.getValue().getQuantity(), entry.getValue().getPrice());
					textField_10.removeAll();

					try {
						// model1.loadProducts();
						model1.addProduct(p1);
						model1.fireTableDataChanged();
					} catch (ArrayIndexOutOfBoundsException e) {
						// } catch (ClassNotFoundException e)
						// {e.printStackTrace();}
						// catch (IOException e) {e.printStackTrace();}

					}
				}
			}
		});

		btnRemoveRow_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				W.removeProduct(textField_9.getText());

				for (int x = 0; x < model1.getWarehouse().size(); x++) {
					if ((textField_9.getText()).equals(model1.getWarehouse().get(x).getName())) {

						setTotalStock(getTotalStock() - model1.getWarehouse().get(x).getQuantity());
						if (getTotalStock() < 0)
							setTotalStock(0);

						model1.getWarehouse().remove(x);
						model1.fireTableDataChanged();

						if (getTotalStock() < 3000) {
							label.setText("");
						}

					}
				}
				model1.storeProducts(model1.getWarehouse());
				model1.fireTableDataChanged();
			}
		});

		textField_10.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void changedUpdate(DocumentEvent arg0) {
				newFilter1();
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) {
				newFilter1();
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) {
				newFilter1();
			}

		});

		JPanel customerPanel = new JPanel();
		customerPanel.setBackground(new Color(144, 238, 144));
		tabbedPane.addTab("Order/Purchase Products", null, customerPanel, null);
		customerPanel.setLayout(null);

		JList list = new JList(model.getCustomers().toArray());
		list.setBackground(Color.WHITE);

		list.setBounds(26, 11, 243, 145);
		customerPanel.add(list);

		JList list_1 = new JList(model1.getWarehouse().toArray());
		list_1.setBackground(Color.WHITE);
		list_1.setBounds(297, 11, 243, 145);
		customerPanel.add(list_1);

		textField_2 = new JTextField();
		textField_2.setBounds(162, 274, 86, 20);
		customerPanel.add(textField_2);
		textField_2.setColumns(10);

		JButton btnOrder = new JButton("Order");

		btnOrder.setForeground(Color.BLACK);
		btnOrder.setBackground(new Color(169, 169, 169));
		btnOrder.setBounds(427, 273, 110, 23);
		customerPanel.add(btnOrder);

		JLabel lblAmount = new JLabel("Amount");
		lblAmount.setBounds(75, 277, 62, 14);
		customerPanel.add(lblAmount);

		textField_3 = new JTextField();
		textField_3.setBounds(162, 230, 86, 20);
		customerPanel.add(textField_3);
		textField_3.setColumns(10);

		textField_4 = new JTextField();
		textField_4.setBounds(162, 186, 86, 20);
		customerPanel.add(textField_4);
		textField_4.setColumns(10);

		JLabel lblClientId = new JLabel("Client ID");
		lblClientId.setBounds(75, 189, 62, 14);
		customerPanel.add(lblClientId);

		JLabel lblProduct = new JLabel("Product");
		lblProduct.setBounds(75, 233, 62, 14);
		customerPanel.add(lblProduct);

		JButton btnAddToCart = new JButton("Add to cart");

		btnAddToCart.setBackground(new Color(169, 169, 169));
		btnAddToCart.setBounds(307, 273, 110, 23);
		customerPanel.add(btnAddToCart);

		JLabel lblNewLabel = new JLabel(" ");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setForeground(new Color(255, 0, 0));
		lblNewLabel.setBounds(75, 302, 194, 14);
		customerPanel.add(lblNewLabel);

		TreeMap<Integer, Order> OPDept = new TreeMap<Integer, Order>();
		OPDept OP = new OPDept(OPDept);

		DefaultListModel mdl = new DefaultListModel();
		JList list_2 = new JList(mdl);
		list_2.setBackground(Color.WHITE);
		list_2.setBounds(297, 167, 240, 89);
		customerPanel.add(list_2);

		btnAddToCart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String fname = " ";
				String lname = " ";
				String category = " ";
				int price = 0;
				boolean customerFound = false;
				boolean productFound = false;

				for (int x = 0; x < model.getCustomers().size(); x++) {
					if (Integer.parseInt(textField_4.getText()) == model.getCustomers().get(x).getCustomerId()) {
						fname = model.getCustomers().get(x).getFirstName();
						lname = model.getCustomers().get(x).getLastName();
						customerFound = true;
					}
				}

				for (Map.Entry<String, Product> entry : W.getWarehouse().entrySet()) {

					if (textField_3.getText().equals(entry.getKey())) {

						if (Integer.parseInt(textField_2.getText()) > entry.getValue().getQuantity())
							lblNewLabel.setText("Warehouse Understock");

						else {
							entry.getValue().setQuantity(
									entry.getValue().getQuantity() - Integer.parseInt(textField_2.getText()));
							productFound = true;
							category = entry.getValue().getCategory();
							price = entry.getValue().getPrice();
							lblNewLabel.setText(" ");
							setBillPrice(getBillPrice() + Integer.parseInt(textField_2.getText()) * price);

						}
					}
				}

				if (customerFound && productFound) {
					Customer C = new Customer(fname, lname, Integer.parseInt(textField_4.getText()));
					Product p = new Product(textField_3.getText(), category, Integer.parseInt(textField_2.getText()),
							price);
					Order o = new Order(C, p);
					OP.addOrder(o);
					mdl.addElement(o);
				}

			}
		});

		btnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				PrintWriter writer;
				String fname = " ";
				String lname = " ";

				SimpleDateFormat time_formatter = new SimpleDateFormat("yyyy-MM-dd - HH:mm:ss");
				String current_time_str = time_formatter.format(System.currentTimeMillis());

				for (int x = 0; x < model.getCustomers().size(); x++) {
					if (Integer.parseInt(textField_4.getText()) == model.getCustomers().get(x).getCustomerId()) {
						fname = model.getCustomers().get(x).getFirstName();
						lname = model.getCustomers().get(x).getLastName();
					}
				}

				try {
					writer = new PrintWriter("chitanta.txt", "UTF-8");

					writer.println("");
					writer.println("Client name: " + fname + " " + lname);
					writer.println("");
					writer.println("-------------------------------------------");
					writer.println("");
					writer.println("Cart: ");
					writer.println("");

					for (int x = 0; x < mdl.getSize(); x++) {
						writer.println(mdl.get(x));
					}
					writer.println("");
					writer.println("-------------------------------------------");
					writer.println("");
					writer.println("Raw sum: " + getBillPrice() + "$");
					writer.println("VAT: " + 0.2 * getBillPrice() + "$");
					writer.println("Total amount: " + 1.2 * getBillPrice() + "$");
					writer.println("-------------------------------------------");
					writer.println("");
					writer.println("Transaction time: " + current_time_str);

					mdl.clear();

					writer.close();

				} catch (FileNotFoundException e) {
				} catch (UnsupportedEncodingException e) {
				}

			}
		});
	}

	public JTextField getFilterText() {
		return filterText;
	}

	public void newFilter() {
		RowFilter<CustomerModel, Integer> rf = null;

		// If current expression doesn't parse, don't update.
		try {
			rf = RowFilter.regexFilter(getFilterText().getText(), 0);
		} catch (java.util.regex.PatternSyntaxException e) {
			return;
		}

		sorter.setRowFilter(rf);
	}

	public JTextField getTextField_10() {
		return textField_10;
	}

	public void newFilter1() {
		RowFilter<ProductModel, Integer> rf1 = null;

		// If current expression doesn't parse, don't update.
		try {
			rf1 = RowFilter.regexFilter(getTextField_10().getText(), 1);
		} catch (java.util.regex.PatternSyntaxException e) {
			return;
		}

		sorter1.setRowFilter(rf1);
	}

	public JTabbedPane getTabbedPane() {
		return tabbedPane;
	}

	public static int getBillPrice() {
		return billPrice;
	}

	public static void setBillPrice(int billPrice) {
		OrderManagement.billPrice = billPrice;
	}

	public static int getTotalStock() {
		return totalStock;
	}

	public static void setTotalStock(int totalStock) {
		OrderManagement.totalStock = totalStock;
	}

}
