package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JScrollBar;
import javax.swing.JSpinner;
import javax.swing.JSeparator;
import javax.swing.JInternalFrame;
import javax.swing.JToolBar;
/*
 * launches the user selection screen
 * 
 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */
public class Selection extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Selection frame = new Selection();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public Selection() throws ClassNotFoundException, IOException {
		setTitle("Application Launcher");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		OrderManagement x = new OrderManagement();
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(127, 255, 212));
		panel.setBounds(0, 0, 424, 261);
		contentPane.add(panel);
		panel.setLayout(null);

		
		JButton btnAdministrator = new JButton("Administrator");
		
		btnAdministrator.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				x.setVisible(true);	
				setVisible(false);
			}
			
		});
		
		btnAdministrator.setForeground(new Color(0, 0, 0));
		btnAdministrator.setBackground(new Color(106, 90, 205));
		btnAdministrator.setBounds(37, 84, 149, 67);
		panel.add(btnAdministrator);
		
		JButton btnCustomer = new JButton("Customer");
		
		btnCustomer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				x.setVisible(true);	
				x.getTabbedPane().setEnabledAt(0, false);
				x.getTabbedPane().setEnabledAt(1, false);
				x.getTabbedPane().setSelectedIndex(2);
				setVisible(false);
			}
		});
		
		btnCustomer.setForeground(new Color(0, 0, 0));
		btnCustomer.setBackground(new Color(106, 90, 205));
		btnCustomer.setBounds(212, 84, 149, 67);
		panel.add(btnCustomer);
		
		JLabel lblSelectYourUser = new JLabel("Select your user class:");
		lblSelectYourUser.setFont(new Font("Times New Roman", Font.PLAIN, 16));
		lblSelectYourUser.setBounds(140, 32, 201, 24);
		panel.add(lblSelectYourUser);
		
		
		
	
	}
}
