package model;

/*
 * describes the Order class

 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */
public class Order {

	/* an order contains a Customer object, a Product object and a unique identifier */
	private Customer c;
	private Product p;
	private int id;

	/* constructor, getters, setters */
	
	public Order(Customer c, Product p, int id) {
		this.c = c;
		this.p = p;
		this.id = id;
	}

	public Order(Customer c, Product p) {
		this.c = c;
		this.p = p;
	}
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Customer getC() {
		return c;
	}

	public void setC(Customer c) {
		this.c = c;
	}

	public Product getP() {
		return p;
	}

	public void setP(Product p) {
		this.p = p;
	}
/*
	@Override
	public String toString() {
		return "" + c.getFirstName() + " " + c.getLastName() + " - " + p + "";
	}
*/
	@Override
	public String toString() {
		return "" + p + "";
	}
	
}
