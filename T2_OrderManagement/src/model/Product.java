package model;

import java.io.Serializable;

/*
 * describes the Product class
 * 
 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */

public class Product implements Serializable {

	/* the fields represent the attributes of a product: its name, the category it is in, quantity and price */
	private String name;
	private String category;
	private int quantity;
	private int price;
	
	/* constructor, getters, setters */
	
	public Product(String name, String category,int quantity, int price) {
		this.name = name;
		this.category = category;
		this.quantity = quantity;
		this.price = price;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "" + name + " - " + price + " $ " + " x " +  quantity + " pieces ";
	}
	
	
	
}
