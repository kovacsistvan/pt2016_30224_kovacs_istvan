package model;

import java.io.Serializable;
/*
 * describes the Customer class
 * 
 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */
public class Customer implements Serializable{

	/* the fields constitute the attributes of a customer: his name along with a personal ID */
	private String firstName;
	private String lastName;
	private int customerId;
	
	/* constructors, getters, setters */
	
	public Customer(String firstName, String lastName, int customerId) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.customerId = customerId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "" + firstName + " " + lastName + " " + customerId + "";
	}

	
	
}
