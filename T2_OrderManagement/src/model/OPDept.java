package model;

import java.util.TreeMap;

import java.util.Map.Entry;
/*
 * describes the OPDept class
 * 
 * an OPDept contains multiple orders in a treemap and implements the add operation 
 * 
 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */

public class OPDept {
 /* treemap of orders */
	private TreeMap<Integer, Order> OPDept = new TreeMap<Integer, Order>();

	public OPDept(TreeMap<Integer, Order> oPDept) {
		OPDept = OPDept;
	}

	public TreeMap<Integer, Order> getOPDept() {
		return OPDept;
	}
	
	public void addOrder(Order O) {
		// System.out.println(P.getName());
		
		this.OPDept.put(O.getId(), O);
		
		}
	
	public void listOrder() {

		for (Entry<Integer,Order> entry : this.getOPDept().entrySet()) {
	
			System.out.println(entry.getValue().getC() + "\n" + entry.getValue().getP() + "\n" + 
							   entry.getValue().getId() + "\n");

		}
	}

	public static void main(String[] args){
		
	Customer C = new Customer("aassds","bihuhkg",3);	
	Customer C1 = new Customer("afghs","bihhjkg",3);
	Product P = new Product("a","b",1,2);
	Product Pq = new Product("abb","dddb",1,2);
	TreeMap<Integer, Order> ss = new TreeMap<Integer, Order>();
	OPDept O = new OPDept(ss);
	
	Order o = new Order(C,P,5);
	Order o1 = new Order(C1,Pq,7);
	O.addOrder(o);
	O.addOrder(o1);
	O.listOrder();
	}
}
