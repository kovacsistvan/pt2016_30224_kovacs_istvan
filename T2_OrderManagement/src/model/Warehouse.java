package model;

import java.util.Map.Entry;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/*
 * describes the Warehouse class
 * 
 * a warehouse contains multiple products in a treemap and implements the add and remove operations 
 * 
 * Kovacs Istvan Norbert, kovacsistvan1202@gmail.com
 */

public class Warehouse implements Serializable {

	/* treemap of products */
	private TreeMap<String, Product> warehouse = new TreeMap<String, Product>();
	public static final int WAREHOUSE_THRESHOLD = 1000;

	public Warehouse(TreeMap<String, Product> warehouse) {
		this.warehouse = warehouse;
	}

	public TreeMap<String, Product> getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(TreeMap<String, Product> warehouse) {
		this.warehouse = warehouse;
	}

	/*
	 * removes the product with the given key
	 * @param key -> product to delete
	 */
	public void removeProduct(String P) {
		boolean modify = false;
		for (Map.Entry<String, Product> entry : this.getWarehouse().entrySet()) {
			if (P.equals(entry.getKey())) {
				modify = true;
			}
		}
		if (modify)
			this.getWarehouse().remove(P);
	}

	/*
	 * adds a product to the treemap
	 * checks for warehouse overstock
	 * @param product --> product to add
	 * @returns a boolean value according to the state of the current stock (true for <3000 , false for > 3000)
	 */
	public boolean addProduct(Product P) {
		// System.out.println(P.getName());

		for (Entry<String, Product> entry : this.getWarehouse().entrySet()) {
			if (P.getName().equals(entry.getKey())) {
				if (P.getQuantity() + entry.getValue().getQuantity() < WAREHOUSE_THRESHOLD)
					P.setQuantity(P.getQuantity() + entry.getValue().getQuantity());
				else {
					return false;
				}
			}
		}

		if (P.getQuantity() < WAREHOUSE_THRESHOLD) {
			this.warehouse.put(P.getName(), P);
			return true;
		}

		else {
			return false;
		}

	}
	/* currently unused, iterates through the treemap and lists its contents */
	public void listInventory() {

		for (Entry<String, Product> entry : this.getWarehouse().entrySet()) {
			String key = entry.getKey();
			Product thing = entry.getValue();

			System.out.println(thing + "\n");

		}
	}
}
