package tests;

import static org.junit.Assert.*;

import java.util.TreeMap;
import java.util.Map.Entry;

import org.junit.Test;

import model.Customer;
import model.Product;
import model.Warehouse;
import model.Order;
import model.OPDept;

public class TestCase {

	/* inserting a quantity under 1000 will return false because of warehouse overstock */
	@Test
	public void productAddTest() {
		Product p = new Product("bread","food",20,20);
		TreeMap<String, Product> warehouse = new TreeMap<String, Product>();
		Warehouse W = new Warehouse(warehouse);
		assertTrue(W.addProduct(p));
	}
	
	/* inserting a quantity over 1000 will return false because of warehouse overstock */
	@Test
	public void productAddOverstockTest() {
		Product p = new Product("bread","food",2000,20);
		TreeMap<String, Product> warehouse = new TreeMap<String, Product>();
		Warehouse W = new Warehouse(warehouse);
		assertFalse(W.addProduct(p));
	}
	
	@Test
	public void orderAddTest() {
		
		Customer C = new Customer("aassds","bihuhkg",3);	
		Customer C1 = new Customer("afghs","bihhjkg",3);
		Product P = new Product("a","b",1,2);
		Product Pq = new Product("abb","dddb",1,2);
		TreeMap<Integer, Order> ss = new TreeMap<Integer, Order>();
		OPDept O = new OPDept(ss);
		
		Order o = new Order(C,P,5);
		Order o1 = new Order(C1,Pq,7);
		O.addOrder(o);
		O.addOrder(o1);
		O.listOrder();
		
		for (Entry<Integer,Order> entry : ss.entrySet()) {

			assertTrue(entry.getValue().getId() != 0);

		}
	}

}
