package controller;

/*
 * 
 * implements the operations between Polynoms: Addition, subtraction, multiplication, derivatives and integrals
 * 
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */
import java.util.ArrayList;

import model.Monom;
import model.MonomDouble;
import model.Polynom;

public class PolyCalculator {

	/*
	 * integrates the entered polynom and returns the result param: input
	 * Polynom X, output Polynom C return: Polynom C
	 */
	public Polynom polyIntegrate(Polynom X, Polynom C) {

		for (Monom s : X.getMonoms()) {

			if (s.getPower() != 0) {

				MonomDouble d = new MonomDouble((double) s.getCoeff() / (double) (s.getPower() + 1), s.getPower() + 1);

				C.addMonom(C, d);
			}
		}

		return C;
	}

	/*
	 * derivates the entered polynom and returns the result param: input Polynom
	 * X, output Polynom C return: Polynom C
	 */
	public Polynom polyDerivate(Polynom X, Polynom C) {

		for (Monom s : X.getMonoms()) {

			Monom d = new Monom(s.getCoeff() * s.getPower(), s.getPower() - 1);
			C.addMonom(C, d);

		}

		return C;
	}

	/*
	 * multiplies the 2 entered polynoms param: input Polynom X,Y, output
	 * Polynom C return: Polynom C
	 */
	public Polynom polyMulti(Polynom X, Polynom Y, Polynom C) {
		int aux = 0, i = 0, sizeA = 0, sizeB = 0;
		ArrayList<Integer> v = new ArrayList<Integer>();

		for (Monom s : X.getMonoms()) {

			if (sizeA < s.getPower())
				sizeA = s.getPower();
		}

		for (Monom s : Y.getMonoms()) {

			if (sizeB < s.getPower())
				sizeB = s.getPower();

		}

		for (i = 0; i <= (sizeA + sizeB); i++) {
			v.add(i, 0);
		}

		for (Monom s : X.getMonoms()) {
			for (Monom d : Y.getMonoms()) {

				aux = s.getCoeff() * d.getCoeff();
				v.set(s.getPower() + d.getPower(), v.get(s.getPower() + d.getPower()) + aux);
			}
		}

		for (i = 0; i <= (sizeA + sizeB); i++) {
			Monom m = new Monom(v.get(i), i);
			C.addMonom(C, m);
		}

		return C;
	}

	/*
	 * adds the 2 entered polynoms param: input Polynom X,Y, output Polynom C
	 * return: Polynom C
	 */
	public Polynom polySum(Polynom X, Polynom Y, Polynom C) {

		int counter = 0;
		for (Monom s : X.getMonoms()) {
			counter = 0;
			for (Monom d : Y.getMonoms()) {
				if (s.getPower() == d.getPower()) {
					counter++;
					Monom m = new Monom(s.getCoeff() + d.getCoeff(), s.getPower());

					C.addMonom(C, m);
				}
			}
			if (counter == 0) {
				Monom m1 = new Monom(s.getCoeff(), s.getPower());
				C.addMonom(C, m1);
				counter = 1;
			}
		}

		for (Monom s : Y.getMonoms()) {
			counter = 0;
			for (Monom d : X.getMonoms()) {
				if (s.getPower() != d.getPower()) {
					counter++;
				}
				if (counter == X.getMonoms().size()) {
					Monom m1 = new Monom(s.getCoeff(), s.getPower());
					C.addMonom(C, m1);
				}
			}
		}

		return C;
	}

	/*
	 * subtracts the 2 entered polynoms param: input Polynom X,Y, output Polynom
	 * C return: Polynom C
	 */
	public Polynom polyDif(Polynom X, Polynom Y, Polynom C) {

		int counter = 0;
		try {

			for (Monom s : X.getMonoms()) {
				counter = 0;
				for (Monom d : Y.getMonoms()) {
					if (s.getPower() == d.getPower()) {
						counter++;
						Monom m = new Monom(s.getCoeff() - d.getCoeff(), s.getPower());
						C.addMonom(C, m);
					}
				}

				if (counter == 0) {
					Monom m1 = new Monom(s.getCoeff(), s.getPower());
					C.addMonom(C, m1);
					counter = 1;
				}

			}

			for (Monom s : Y.getMonoms()) {
				counter = 0;
				for (Monom d : X.getMonoms()) {
					if (s.getPower() != d.getPower()) {
						counter++;
					}
					if (counter == X.getMonoms().size()) {
						Monom m1 = new Monom(-s.getCoeff(), s.getPower());
						C.addMonom(C, m1);
					}
				}
			}
		}

		catch (Throwable e) {
		}

		return C;
	}

	/*
	 * divides the 2 entered polynoms not implemented param: input Polynom A,B,
	 * output Polynom C return: Polynom C
	 */
	public Polynom polyDivide(Polynom A, Polynom B, Polynom C, Polynom D) {

		return C;
	}

}
