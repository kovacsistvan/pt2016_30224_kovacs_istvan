package view;

/*
 * Listener for the operation buttons
 * picks between responses according to the ActionCommands set in the PolyWindow
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */

import java.awt.event.*;
import javax.swing.*;
import controller.PolyCalculator;
import model.Polynom;

public class OperationButtons implements ActionListener {

JLabel rezultat;
Polynom X,Y;
	
	public OperationButtons(Polynom X, Polynom Y, JLabel rezultat) {
	this.X=X;
	this.Y=Y;
	this.rezultat=rezultat;
}

	public void actionPerformed(ActionEvent e){ 
		
		int action=Integer.parseInt(e.getActionCommand());
		
		PolyCalculator calc = new PolyCalculator();
		Polynom C = new Polynom();
		Polynom D = new Polynom();
		
		switch (action) {
		case 1: C=calc.polySum(X,Y,C);	
			break;
		case 2: C=calc.polyDif(X,Y,C);
			break;
		case 3: C=calc.polyMulti(X,Y,C);
			break;
		case 4: C=calc.polyDivide(X,Y,C,D);
			break;
		case 5: C=calc.polyDerivate(X,C);
			break;
		case 6: C=calc.polyIntegrate(X,C);
			break;
		default:
			break;
		}

		if (action!=6)
		
		rezultat.setText(C.listMonoms(C.getMonoms()));
		
		else
			
		rezultat.setText(C.listDoubleMonoms(C.getDoubleMonoms()));
	}
}

