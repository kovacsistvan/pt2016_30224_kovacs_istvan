package view;

/*
 * 
 * sets up the Graphical User Interface for the application
 * initializes the 2 Polynoms used in the application with their values
 * calls the actionListeners in charge of operations, adding Monoms, etc.
 * 
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;

import model.Polynom;

import java.awt.Toolkit;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;

@SuppressWarnings("serial")
public class PolyWindow extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PolyWindow frame = new PolyWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Frame created using WindowBuilder
	 */
	public PolyWindow() {

		Polynom A = new Polynom();
		Polynom B = new Polynom();

		setTitle("Polynom Calculator");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\user\\Desktop\\1456924102208.png"));
		setBackground(new Color(0, 204, 0));
		setForeground(new Color(0, 204, 0));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 798, 346);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 153));
		contentPane.setForeground(new Color(0, 204, 0));
		contentPane.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null, null));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel labelCoeff = new JLabel("Coeficient");
		labelCoeff.setBackground(new Color(0, 0, 0));
		labelCoeff.setBounds(26, 23, 66, 14);
		contentPane.add(labelCoeff);

		JLabel labelPower = new JLabel("Putere");
		labelPower.setBounds(26, 69, 54, 14);
		contentPane.add(labelPower);

		textField = new JTextField();
		textField.setBackground(new Color(255, 255, 255));
		textField.setBounds(102, 20, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(102, 66, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		JButton btnSubmitA = new JButton("Submit A");
		btnSubmitA.setForeground(new Color(0, 0, 0));
		btnSubmitA.setBackground(new Color(204, 204, 255));
		btnSubmitA.setBounds(10, 107, 86, 23);
		contentPane.add(btnSubmitA);

		JButton btnSubmitB = new JButton("Submit B");
		btnSubmitB.setForeground(new Color(0, 0, 0));
		btnSubmitB.setBackground(new Color(204, 204, 255));
		btnSubmitB.setBounds(102, 107, 86, 23);
		contentPane.add(btnSubmitB);

		JLabel labelA = new JLabel("Polinomul A:");
		labelA.setBounds(198, 23, 75, 14);
		contentPane.add(labelA);

		JLabel labelB = new JLabel("Polinomul B:");
		labelB.setBounds(198, 69, 75, 14);
		contentPane.add(labelB);

		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setBounds(275, 23, 482, 14);
		contentPane.add(lblNewLabel_4);

		JLabel lblNewLabel_5 = new JLabel("");
		lblNewLabel_5.setBounds(275, 69, 482, 14);
		contentPane.add(lblNewLabel_5);

		JLabel labelOp = new JLabel("Operatii:");
		labelOp.setBounds(10, 191, 62, 14);
		contentPane.add(labelOp);

		JButton btnPlus = new JButton("+");
		btnPlus.setForeground(new Color(0, 0, 0));
		btnPlus.setBackground(new Color(204, 204, 255));
		btnPlus.setBounds(82, 183, 46, 30);
		contentPane.add(btnPlus);

		JButton buttonMinus = new JButton("-");
		buttonMinus.setForeground(new Color(0, 0, 0));
		buttonMinus.setBackground(new Color(204, 204, 255));
		buttonMinus.setBounds(138, 183, 43, 30);
		contentPane.add(buttonMinus);

		JButton buttonMulti = new JButton("*");
		buttonMulti.setForeground(new Color(0, 0, 0));
		buttonMulti.setBackground(new Color(204, 204, 255));
		buttonMulti.setBounds(191, 183, 43, 30);
		contentPane.add(buttonMulti);

		JButton buttonDiv = new JButton("/");
		buttonDiv.setForeground(new Color(0, 0, 0));
		buttonDiv.setBackground(new Color(204, 204, 255));
		buttonDiv.setBounds(244, 183, 43, 30);
		contentPane.add(buttonDiv);

		JButton buttonDeri = new JButton("'");
		buttonDeri.setForeground(new Color(0, 0, 0));
		buttonDeri.setBackground(new Color(204, 204, 255));
		buttonDeri.setBounds(297, 183, 43, 30);
		contentPane.add(buttonDeri);

		JButton buttonIntegr = new JButton("S");
		buttonIntegr.setForeground(new Color(0, 0, 0));
		buttonIntegr.setBackground(new Color(204, 204, 255));
		buttonIntegr.setBounds(350, 183, 43, 30);
		contentPane.add(buttonIntegr);

		JLabel labelResult = new JLabel("Rezultat:");
		labelResult.setBounds(10, 234, 70, 30);
		contentPane.add(labelResult);

		JLabel lblNewLabel_8 = new JLabel("");
		lblNewLabel_8.setBounds(82, 238, 675, 26);
		contentPane.add(lblNewLabel_8);

		JButton btnResetA = new JButton("Reset A");
		btnResetA.setForeground(new Color(0, 0, 0));
		btnResetA.setBackground(new Color(204, 204, 255));
		btnResetA.setBounds(10, 141, 86, 23);
		contentPane.add(btnResetA);

		JButton btnResetB = new JButton("Reset B");
		btnResetB.setForeground(new Color(0, 0, 0));
		btnResetB.setBackground(new Color(204, 204, 255));
		btnResetB.setBounds(102, 141, 86, 23);
		contentPane.add(btnResetB);

		ButtonListeners a = new ButtonListeners(A, textField, textField_1, lblNewLabel_4);
		ButtonListeners b = new ButtonListeners(B, textField, textField_1, lblNewLabel_5);

		btnSubmitA.addActionListener(a);
		btnSubmitB.addActionListener(b);
		btnSubmitA.setActionCommand("1");
		btnSubmitB.setActionCommand("1");

		btnResetA.addActionListener(a);
		btnResetB.addActionListener(b);
		btnResetA.setActionCommand("2");
		btnResetB.setActionCommand("2");

		OperationButtons c = new OperationButtons(A, B, lblNewLabel_8);
		btnPlus.addActionListener(c);
		buttonMinus.addActionListener(c);
		buttonMulti.addActionListener(c);
		
		buttonDeri.addActionListener(c);
		buttonIntegr.addActionListener(c);
		btnPlus.setActionCommand("1");
		buttonMinus.setActionCommand("2");
		buttonMulti.setActionCommand("3");

		buttonDeri.setActionCommand("5");
		buttonIntegr.setActionCommand("6");

	}
}
