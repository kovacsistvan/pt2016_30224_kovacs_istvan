package view;

/*
 * Listener for the add, submit and reset buttons
 * picks between responses according to the ActionCommands set in the PolyWindow
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */


import java.awt.event.*;

import javax.swing.*;

import model.Monom;
import model.MonomDouble;
import model.Polynom;

public class ButtonListeners implements ActionListener {

	JLabel polinomA;
	JTextField powert;
	JTextField coefft;
	Polynom X;

	public ButtonListeners(Polynom X, JTextField coefft, JTextField powert, JLabel polinomA) {
		this.X = X;
		this.coefft = coefft;
		this.powert = powert;
		this.polinomA = polinomA;
	}

	public void actionPerformed(ActionEvent e) {

		int action = Integer.parseInt(e.getActionCommand());
		int i = 0;
		
		Monom m = new Monom(Integer.parseInt(coefft.getText()), Integer.parseInt(powert.getText()));
		MonomDouble md = new MonomDouble(Integer.parseInt(coefft.getText()), Integer.parseInt(powert.getText()));
		
		X.addMonom(X, m);
		X.addMonom(X, md);
		

		try {

			if (action == 2) {

				while (i <= X.getMonoms().size()) {
					X.getMonoms().remove(i);
				}
			}

		}

		catch (Throwable e1) {
		}

		polinomA.setText(X.listMonoms(X.getMonoms()));

	}
}
