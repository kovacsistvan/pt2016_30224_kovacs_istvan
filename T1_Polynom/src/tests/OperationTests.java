package tests;

/*
 * 
 * tests the conditional constructions from the methods
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */

import static org.junit.Assert.*;

import org.junit.Test;
import model.Monom;
import model.Polynom;
import controller.PolyCalculator;

public class OperationTests {

	/* PolyIntegrate test*/
	@Test
	public void testPolyIntegrate() {
	Monom x = new Monom(2,3);
	
	assertTrue(x.getPower() != 0);
	}
	
	/* PolyMulti test*/
	@Test
	public void testPolyMulti() {
	Monom x = new Monom(2,3);
	int sizeA=2;
	
	assertTrue(sizeA < x.getPower());
	}
	
	/* PolyDerivate test*/
	@Test
	public void testPolyDerivate() {
	Monom x = new Monom(2,3);
	
	assertTrue(x.getPower() != 0);
	}
	
	/* PolySum test*/
	@Test
	public void testPolySum() {
	Monom x = new Monom(2,3);
	Monom y = new Monom(3,3);
	Monom z = new Monom(2,4);
	Polynom X = new Polynom();
	Polynom Y = new Polynom();
	Polynom result = new Polynom();
	PolyCalculator calc = new PolyCalculator();
	X.addMonom(X, x);
	Y.addMonom(Y, y);
	result=calc.polySum(X,Y,result);
	
	assertTrue(result.getMonoms().get(0).getPower() == x.getPower());
	assertTrue(result.getMonoms().get(0).getCoeff() == x.getCoeff() + y.getCoeff());
	assertTrue (x.getPower() == y.getPower());
	assertFalse(x.getPower() == z.getPower());
	}
	
	/* PolyDif test*/
	@Test
	public void testPolyDif() {
	Monom x = new Monom(7,3);
	Monom y = new Monom(5,3);
	int counter=1;
	
	Polynom X = new Polynom();
	Polynom Y = new Polynom();
	Polynom resultD = new Polynom();
	PolyCalculator calc = new PolyCalculator();
	X.addMonom(X, x);
	Y.addMonom(Y, y);
	resultD=calc.polyDif(X,Y,resultD);
	
	assertTrue(resultD.getMonoms().get(0).getPower() == x.getPower());
	assertTrue(resultD.getMonoms().get(0).getCoeff() == x.getCoeff() - y.getCoeff());
	
	//assertTrue (x.getPower() != y.getPower());
	assertTrue(counter == X.getMonoms().size());
	
	}
	
	/* ListMonoms test*/
	@Test
	public void testListMonoms() {
	Monom x = new Monom(7,5);
	Monom y = new Monom(5,3);
	assertTrue (x.getPower() > y.getPower());
	assertTrue(x.getCoeff() != 0);
	
	}

}
