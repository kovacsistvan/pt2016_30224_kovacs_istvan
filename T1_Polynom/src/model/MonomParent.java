package model;

/*
 * 
 * declares the main Monom class which extends Monom and MonomDouble
 * establishes the constructors and getters/setters
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */

public class MonomParent{
	/* power of the monom */
	public int power;

	public MonomParent(int power) {
		this.power = power;
	}
	
	public int getPower() {
		return power;
	}

	public void setPower(int power) {
		this.power = power;
	}
}
