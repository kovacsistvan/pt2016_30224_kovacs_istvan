package model;

/*
 * 
 * inherits the power int from the MonomParent class
 * sets up getters and setters and the constructor for the int coefficient Monoms
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */

public class Monom extends MonomParent{
/* coefficient of the Monom */
	private int coeff;

	public Monom(int coeff,int power) {
		super(power);
		this.coeff = coeff;
		this.power = power;
	}

	public int getCoeff() {
		return coeff;
	}

	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}
}
