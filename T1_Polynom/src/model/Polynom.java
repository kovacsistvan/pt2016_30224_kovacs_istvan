package model;

/*
 * Declares the Polynom object, which consists of an ArrayList of Monoms or, alternatively, MonomDoubles
 * also declares listing methods for the polynoms, along with getters and setters
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */

import java.util.*;

public class Polynom {
	/* ArrayList of Monoms*/
	private ArrayList<Monom> monoms = new ArrayList<Monom>();
	/* ArrayList of MonomDoubles*/
	private ArrayList<MonomDouble> doubleMonoms = new ArrayList<MonomDouble>();

	/*
	 * param: the ArrayList of Monoms that we want to display
	 * returns a String called output
	 * 
	 * this method is in charge of sorting the Polynoms according to their power 
	 * also "cleans up" monoms which have null coefficients
	 * 
	 */
	public String listMonoms(ArrayList<Monom> z) {

		String output = "";
		int aux1, aux2;
		for (Monom s : z) {
			for (Monom d : z) {
				if (s.getPower() > d.getPower()) {
					aux1 = s.getPower();
					aux2 = s.getCoeff();
					s.setPower(d.getPower());
					s.setCoeff(d.getCoeff());
					d.setPower(aux1);
					d.setCoeff(aux2);
				}
			}
		}
		
		for (Monom s : z) {
			if (s.getCoeff() != 0) {
				output = output + "" + s.getCoeff() + "x^" + s.getPower() + "  ";
			}
		}
		// System.out.println(output);
		return output;
	}
	
	/*
	 * param: the ArrayList of MonomDoubles that we want to display
	 * returns a String called output
	 * 
	 * does exactly what listMonoms does, except for Polynoms with MonomDoubles
	 * 
	 */
	public String listDoubleMonoms(ArrayList<MonomDouble> z) {

		String output = "";
		int aux1;
		double aux2;
		for (MonomDouble s : z) {
		s.setCoeff(s.getCoeff());
			for (MonomDouble d : z) {
				if (s.getPower() > d.getPower()) {
					aux1 = s.getPower();
					aux2 = s.getCoeff();
					s.setPower(d.getPower());
					s.setCoeff(d.getCoeff());
					d.setPower(aux1);
					d.setCoeff(aux2);
					
				}
			}
		}
		
		for (MonomDouble s : z) {
			if (s.getCoeff() != 0) {
				output = output + "" + s.getCoeff() + "x^" + s.getPower() + "  ";
			}
		}
		// System.out.println(output);
		return output;
	}
	
	/* adds a Monom to the Polynom
	 * param: a Polynom and the Monom we want to add
	 */
	public void addMonom(Polynom X, Monom x) {
		X.monoms.add(x);

	}
	
	/* adds a MonomDouble to the Polynom
	 * param: a Polynom and the MonomDouble we want to add
	 */
	public void addMonom(Polynom X, MonomDouble x) {
		X.doubleMonoms.add(x);
	
	}

	/* getters and setters */
	public ArrayList<Monom> getMonoms() {
		return monoms;
	}

	public ArrayList<MonomDouble> getDoubleMonoms() {
		return doubleMonoms;
	}

	public void setMonoms(ArrayList<Monom> monoms) {
		this.monoms = monoms;
	}

	public void setDoubleMonoms(ArrayList<MonomDouble> doubleMonoms) {
		this.doubleMonoms = doubleMonoms;
	}
}
