package model;

/*
 * 
 * inherits the power int from the MonomParent class
 * sets up getters and setters and the constructor for the double coefficient Monoms used for integrals
 * 
 * @author Kovacs Istvan, kovacsistvan1202@gmail.com
 */

public class MonomDouble extends MonomParent{
	/* coefficient of the Monom */
	private double coeff;

	public MonomDouble(double coeff, int power) {
			super(power);
			this.coeff=coeff;
			this.power= power;
	}

	public double getCoeff() {
		return coeff;
	}

	public void setCoeff(double coeff) {
		this.coeff = coeff;
	}
	
}
