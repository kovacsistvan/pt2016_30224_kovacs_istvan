package controller;

import java.util.HashMap;

import javax.swing.DefaultListModel;

public class ProxyDictionary implements Dictionary {

	private LanguageDictionary realDictionary;
	private HashMap<String, String> dictionary = new HashMap<String, String>();
	
	public ProxyDictionary() {
	}

	@Override
	public void addWord(String word, String description) {
		
		if(realDictionary == null){
			realDictionary = LanguageDictionary.getInstance();
			System.out.println("proxy dictionary");
	      }
		realDictionary.addWord(word, description);

	}

	@Override
	public void removeWord(String word) {
		// TODO Auto-generated method stub

	}

	@Override
	public String searchWords(String word) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String displayDescription(String word) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void refresh(DefaultListModel<String> model) {
		// TODO Auto-generated method stub

	}

}
