package controller;

import javax.swing.DefaultListModel;

public interface Dictionary {

	
	/**
	 * 
	 * @param word
	 * @param description
	 * @pre word != null && word != null;
	 * @post word != null
	 */
	public void addWord(String word, String description);
	
	/**
	 * 
	 * @param word
	 * @param description
	 * @pre word != null && description != null;
	 * @post word != null
	 */
	public void removeWord(String word);
	/**
	 * 
	 * @param word
	 * @pre word != null;
	 * @post word != null
	 */
	public String searchWords(String word);
	/**
	 * 
	 * @param word
	 * @pre word != null;
	 * @post word != null
	 */
	public String displayDescription(String word);
	
	/**
	 * 
	 * @param model
	 * @pre model != null;
	 * @post model != null
	 */
	public void refresh(DefaultListModel<String> model);
}

