package controller;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import model.DictionaryObserver;
import model.Observer;
import javax.swing.DefaultListModel;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;

public class LanguageDictionary implements Dictionary {

	//Singleton
	private static LanguageDictionary instance = new LanguageDictionary();
	
	//Observers
	private ArrayList<Observer> observers = new ArrayList<Observer>();
	private int state;
	
	
	
	private HashMap<String, String> dictionary = new HashMap<String, String>();


	public static LanguageDictionary getInstance(){
	      return instance;
	   }
	
	public boolean isWellFormed(){
		
		if (dictionary != null) return true;
		else return false;
	}
	 
	public void addWord(String word, String description) {
		assert word != null & description != null : "null string";
		assert isWellFormed(): "not well formed";
		
		dictionary.put(word, description);
		
		assert isWellFormed(): "not well formed";
		assert word != null;
	}
	
	public void removeWord(String word) {
		assert word != null: "null string";
		assert isWellFormed(): "not well formed";
		new DictionaryObserver(this);
		dictionary.remove(word);
		this.setState(1);
		assert word != null: "null string";
		assert isWellFormed(): "not well formed";
	}

	public String searchWords(String word){
		assert word != null: "null string";
		assert isWellFormed(): "not well formed";
		word = word.replace('?', '.');
		word = word.replace("*", ".*");
		//System.out.println(word);
		
		Iterator<Map.Entry<String, String>> entries = dictionary.entrySet().iterator();
		
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next();

			if (entry.getKey().matches(word)) {
				System.out.println("Match found: " + entry.getKey());
				return entry.getKey();
			}
		}
		assert word != null: "null string";
		assert isWellFormed(): "not well formed";
		return "notfound";
	}
	
	
	public String displayDescription(String word) {
		assert word != null: "null string";
		assert isWellFormed(): "not well formed";
		String description = "";
		Iterator<Map.Entry<String, String>> entries = dictionary.entrySet().iterator();
		while (entries.hasNext()) {
			Map.Entry<String, String> entry = entries.next();

			if (entry.getKey().equals(word)) {
				description = entry.getValue();
			}
		}
		assert word != null: "null string";
		assert isWellFormed(): "not well formed";
		return description;

	}

	public void refresh(DefaultListModel<String> model) {
		assert model != null: "null model";
		assert isWellFormed(): "not well formed";
		for (String word : dictionary.keySet()) {
			model.addElement(word);
		}	
		assert model != null: "null model";
		assert isWellFormed(): "not well formed";
	}
	
	public void save() {	
		//JSONObject obj = new JSONObject();	
		//obj.putAll(dictionary);
	}
	
	public void load(){		
		 //JSONParser parser = new JSONParser();		 
		// HashMap<String, String> load = new HashMap<String, String>();	 
	}

	public HashMap<String, String> getDictionary() {
		return dictionary;
	}

	public void setDictionary(HashMap<String, String> dictionary) {
		this.dictionary = dictionary;
		notifyAllObservers();
	}
	
	 public int getState() {
	      return state;
	   }

	   public void setState(int state) {
	      this.state = state;
	      notifyAllObservers();
	   }

	   public void attach(Observer observer){
	      observers.add(observer);		
	   }

	   public void notifyAllObservers(){
	      for (Observer observer : observers) {
	         observer.update();
	      }
	   } 
	
}
