package model;

public class SingleDictionary {

	  //create an object of SingleObject
	   private static SingleDictionary instance = new SingleDictionary();

	   //make the constructor private so that this class cannot be
	   //instantiated
	   private SingleDictionary(){}

	   //Get the only object available
	   public static SingleDictionary getInstance(){
	      return instance;
	   }

	   public void showMessage(){
	      System.out.println("Hello World!");
	   }
	}
	

