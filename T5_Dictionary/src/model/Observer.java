package model;

import controller.LanguageDictionary;

public abstract class Observer {
	   protected LanguageDictionary subject;
	   public abstract void update();
	}
