package model;

import controller.LanguageDictionary;

public class DictionaryObserver extends Observer {

	public DictionaryObserver(LanguageDictionary subject){
	      this.subject = subject;
	      this.subject.attach(this);
	   }

	   @Override
	   public void update() {
	      System.out.println( "The state of the HashMap has changed: word removed." ); 
	   }
	}
	

