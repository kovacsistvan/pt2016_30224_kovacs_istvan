package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Document;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

import org.json.simple.JSONObject;

import controller.LanguageDictionary;
import controller.ProxyDictionary;

public class GUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextArea textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI frame = new GUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI() {
		setBackground(new Color(153, 204, 102));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 564, 385);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(204, 204, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		LanguageDictionary D = LanguageDictionary.getInstance();

		//ProxyDictionary D = new ProxyDictionary();

		JList<String> list = new JList<String>();
		DefaultListModel<String> model = new DefaultListModel<String>();
		list.setBounds(10, 11, 177, 294);
		contentPane.add(list);

		JLabel lblWord = new JLabel("Word");
		lblWord.setBounds(197, 12, 46, 14);
		contentPane.add(lblWord);

		JLabel lblDescription = new JLabel("Description");
		lblDescription.setBounds(197, 37, 103, 14);
		contentPane.add(lblDescription);

		JButton btnAdd = new JButton("Add");

		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				D.addWord(textField.getText(), textField_1.getText());
				model.clear();
				D.refresh(model);
				list.setModel(model);

			}
		});

		btnAdd.setBackground(Color.LIGHT_GRAY);
		btnAdd.setBounds(429, 62, 89, 23);
		contentPane.add(btnAdd);

		list.addListSelectionListener(new ListSelectionListener() {

			public void valueChanged(ListSelectionEvent event) {
				if (!event.getValueIsAdjusting()) {
					@SuppressWarnings("unchecked")
					JList<String> source = (JList<String>) event.getSource();

					try {
						String selected = source.getSelectedValue().toString();

						System.out.println(selected);

						textField_2.setText(D.displayDescription(selected));
					} catch (NullPointerException e) {

					}
				}
			}

		});

		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {

				D.removeWord(textField.getText());
				model.clear();
				D.refresh(model);
				list.setModel(model);
			}

		});
		btnRemove.setBackground(Color.LIGHT_GRAY);
		btnRemove.setBounds(429, 96, 89, 23);
		contentPane.add(btnRemove);

		textField = new JTextField();
		textField.setBounds(274, 10, 146, 23);
		contentPane.add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(274, 34, 146, 23);
		contentPane.add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextArea();
		textField_2.setBounds(211, 139, 208, 166);
		contentPane.add(textField_2);
		textField_2.setColumns(10);

		JButton btnSearch = new JButton("Search");
		btnSearch.setBackground(Color.LIGHT_GRAY);

		btnSearch.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				String rez = D.searchWords(textField_3.getText());

				if (rez != "notfound")
					textField_2.setText(rez);
				else
					textField_2.setText("not found");

			}

		});

		btnSearch.setBounds(429, 130, 89, 23);
		contentPane.add(btnSearch);

		JLabel lblSearchTerm = new JLabel("Search Term");
		lblSearchTerm.setBounds(197, 88, 79, 14);
		contentPane.add(lblSearchTerm);

		textField_3 = new JTextField();
		textField_3.setBounds(274, 85, 145, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnSave = new JButton("Save");
		btnSave.setBackground(Color.LIGHT_GRAY);
		btnSave.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				
			//	D.save();
				
			}
		});
		
		btnSave.setBounds(429, 174, 89, 23);
		contentPane.add(btnSave);
		
		JButton btnLoad = new JButton("Load");
		btnLoad.setBackground(Color.LIGHT_GRAY);
		
		btnLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			//	D.load();
				
			}
		});
		
		btnLoad.setBounds(429, 208, 89, 23);
		contentPane.add(btnLoad);
	}
}
